XPINAME = @stylRRR2.xpi

.PHONY: all
all: ${XPINAME}

${XPINAME}:
	(cd src/ && zip -r9 ../${XPINAME} .)

.PHONY: clean
clean:
	rm -f ${XPINAME}

.PHONY: install
install:
ifndef PROFILE
	$(error PROFILE is not defined)
endif
	cp -f ${XPINAME} "${PROFILE}/extensions/${XPINAME}"

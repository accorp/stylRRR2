//// written by Alexander Mehler ////
//// MPL 2.0 license ////

var {Cc, Ci} = require("chrome");

var sdk =
{
	self: require("sdk/self"),
	file: require("sdk/io/file"),
	toggle: require("sdk/ui/button/toggle").ToggleButton,
	panel: require("sdk/panel").Panel,
};

var mozilla =
{
	directoryService: Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties),
	ioService: Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService),
	scriptableInputStream: Cc["@mozilla.org/scriptableinputstream;1"].getService(Ci.nsIScriptableInputStream),
	styleSheetService: Cc["@mozilla.org/content/style-sheet-service;1"].getService(Ci.nsIStyleSheetService),
	windowMediator: Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator),
	filePicker: Cc["@mozilla.org/filepicker;1"].getService(Ci.nsIFilePicker),
};

var extId = "stylRRR2";
var dbJson = "DB.json";

// utils //

function getPath(filename = "")
{
	let path = mozilla.directoryService.get("ProfD", Ci.nsIFile);
	path.append(extId);
	if( filename != "" )
	{
		path.append(filename);
	}
	return path;
}

function cssPath(name)
{
	return getPath(name.trim().replace(/[\|&;\$#%@"<>\(\)\+,\\\*?\/!\´\`:=\{\}\[\]]+/g, "-") + ".css");
}

function readFile(iFile)
{
	return sdk.file.read(iFile.path);
}

function writeFile(iFile, text)
{
	let file = sdk.file.open(iFile.path, "w");
	try
	{
		if( typeof text === 'string' || text instanceof String )
		{
			file.write(text);
		}
		else
		{
			for( str of text )
			{
				file.write(str);
			}
		}
	}
	finally
	{
		file.close();
	}
}

function deleteFile(iFile)
{
	sdk.file.remove(iFile.path);
}

// styles //

function readStyleSheet(name)
{
	return readFile(cssPath(name));
}

function writeStyleSheet(name, text)
{
	writeFile(cssPath(name), text);
}

function deleteStyleSheet(name)
{
	deleteFile(cssPath(name));
}

function registerStyleSheet(style)
{
	if( style.enabled )
	{
		let uri = mozilla.ioService.newURI("file://" + cssPath(style.name).path, null, null);
		mozilla.styleSheetService.loadAndRegisterSheet(uri, mozilla.styleSheetService.AUTHOR_SHEET);
	}
}

function unregisterStyleSheet(style)
{
	if( style.enabled )
	{
		let uri = mozilla.ioService.newURI("file://" + cssPath(style.name).path, null, null);
		mozilla.styleSheetService.unregisterSheet(uri, mozilla.styleSheetService.AUTHOR_SHEET);
	}
}

var styles =
[
	{
		name: "Nicer Youtube",
		author: "Ulf3000",
		url:  "",
		enabled: true,
	},
	{
		name: "Disable UI Animations",
		author: "Ulf3000",
		url:  "",
		enabled: false,
	},
	{
		name: "Style the Styler",
		author: "accorp",
		url:  "",
		enabled: false,
	},
];

function readResource(path)
{
	let channel = mozilla.ioService.newChannel2(sdk.self.data.url(path), null, null, null, null, null, null, null);
	let input = channel.open();
	try
	{
		mozilla.scriptableInputStream.init(input);
		try
		{
			return mozilla.scriptableInputStream.read(input.available());
		}
		finally
		{
			mozilla.scriptableInputStream.close();
		}
	}
	finally
	{
		input.close();
	}
}

function copyExampleStyleSheets()
{
	for( let style of styles )
	{
		let text = readResource("styles/" + style.name + ".css");
		writeStyleSheet(style.name, text);
	}
}

function registerAllStyleSheets()
{
	for( let style of styles )
	{
		registerStyleSheet(style);
	}
}

function unregisterAllStyleSheets()
{
	for( let style of styles )
	{
		unregisterStyleSheet(style);
	}
}

// storage //

function readStorage()
{
	let text = readFile(getPath(dbJson));
	styles = JSON.parse(text);
}

function writeStorage()
{
	writeFile(getPath(dbJson), [JSON.stringify(styles, null, 4), "\n"]);
	stylePanel.port.emit("resize");
}

// UI //

var optionsButton = sdk.toggle({
	id:    "StylRRR2Panel",
	label: "StylRRR2 Panel",
	icon:  "./icon-16.gif",
	onChange: function (state)
	{
		if( state.checked )
		{
			stylePanel.show({position: optionsButton});
		}
	},
});

var stylePanel = sdk.panel({
	width:  610,
	height: 350,
	contextMenu: true,
	contentURL:  "./styler2.html#item1",
	contentScriptFile: "./styler.js",
	onHide: function ()
	{
		optionsButton.state("window", {checked: false});
	},
});

stylePanel.port.on("resize", function ([width, height])
{
	let update = false;
	if( width > 0 && width != stylePanel.width )
	{
		update = true;
	}
	else
	{
		width = stylePanel.width;
	}
	if( height > 0 && height != stylePanel.height )
	{
		update = true;
	}
	else
	{
		height = stylePanel.height;
	}
	if( update )
	{
		stylePanel.resize(width, height);
	}
});

stylePanel.port.on("update", function (index)
{
	let style = styles[index];
	let text = readStyleSheet(style.name)
	stylePanel.port.emit("update", [style, text]);
});

stylePanel.port.on("save", function ([index, author, url, text])
{
	let style = styles[index];
	unregisterStyleSheet(style);
	style.author = author;
	style.url = url;
	writeStyleSheet(style.name, text);
	registerStyleSheet(style);
	writeStorage();
});

stylePanel.port.on("new", function ([name, author, url, text])
{
	let style =
	{
		name: name,
		author: author,
		url: url,
		enabled: true,
	};
	styles.push(style);
	writeStyleSheet(style.name, text);
	registerStyleSheet(style);
	writeStorage();
});

stylePanel.port.on("delete", function (index)
{
	let style = styles[index];
	unregisterStyleSheet(style);
	deleteStyleSheet(style.name);
	styles.splice(index, 1);
	writeStorage();
});

stylePanel.port.on("enabled", function ([index, enabled])
{
	let style = styles[index];
	unregisterStyleSheet(style);
	style.enabled = enabled;
	registerStyleSheet(style);
	writeStorage();
});

stylePanel.port.on("reload", function (index)
{
	let style = styles[index];
	unregisterStyleSheet(style);
	registerStyleSheet(style);
	let text = readStyleSheet(style.name)
	stylePanel.port.emit("update", [style, text]);
});

stylePanel.port.on("load-file", function ()
{
	var window = mozilla.windowMediator.getMostRecentWindow("navigator:browser");
	mozilla.filePicker.init(window, "Load Style Sheet", Ci.nsIFilePicker.modeOpen);
	mozilla.filePicker.appendFilters(Ci.nsIFilePicker.filterAll | Ci.nsIFilePicker.filterText);
	mozilla.filePicker.open(function (rv)
	{
		if( rv == Ci.nsIFilePicker.returnOK || rv == Ci.nsIFilePicker.returnReplace )
		{
			var name = mozilla.filePicker.file.leafName.replace(/\.[^\.]+$/, '');
			var text = readFile(mozilla.filePicker.file);
			stylePanel.port.emit("load-file", [name, text]);
		}
	});
});

stylePanel.port.on("open-folder", function ()
{
	getPath().launch();
});

// start //

function onLoad()
{
	let dir = getPath();
	if( !dir.exists() )
	{
		sdk.file.mkpath(dir.path);
	}
	
	var db = getPath(dbJson);
	if( db.exists() )
	{
		readStorage();
		registerAllStyleSheets();
		stylePanel.port.emit("resize");
	}
	else
	{
		copyExampleStyleSheets();
		registerAllStyleSheets();
		writeStorage();
	}
	
	stylePanel.port.emit("styles", styles);
}

exports.onUnload = function ()
{
	unregisterAllStyleSheets();
};

onLoad();

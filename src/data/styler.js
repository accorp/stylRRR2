var nameBox1 = document.getElementById("nameBox1");
var domainBox1 = document.getElementById("domainBox1");
var urlBox1 = document.getElementById("urlBox1");
var editBox1 = document.getElementById("editBox1");
var saveBtn1 = document.getElementById("saveBtn1");
var delBtn1 = document.getElementById("delBtn1");
var enabled1 = document.getElementById("enabled1");
var reloadBtn1 = document.getElementById("reloadBtn1");
var newBtn1 = document.getElementById("newBtn1");
var openFolder1 = document.getElementById("openFolder1");

var nameBox2 = document.getElementById("nameBox2");
var domainBox2 = document.getElementById("domainBox2");
var urlBox2 = document.getElementById("urlBox2");
var editBox2 = document.getElementById("editBox2");
var saveBtn2 = document.getElementById("saveBtn2");
var cancelBtn2 = document.getElementById("cancelBtn2");
var error2 = document.getElementById("error2");
var loadFile2 = document.getElementById("loadFile2");

var delStyle3 = document.getElementById("delStyle3");
var delBtn3 = document.getElementById("delBtn3");
var cancelBtn3 = document.getElementById("cancelBtn3");

function gotoTab(index)
{
	location.href = location.href.replace(/#item\d+$/g, "#item" + index);
}

self.port.on("styles", function (styles)
{
	let index = nameBox1.selectedIndex;
	for( let i = nameBox1.options.length; i > 0; i -- )
	{
		nameBox1.remove(i - 1);
	}
	for( let style of styles )
	{
		nameBox1.appendChild(new Option(style.name));
	}
	nameBox1.selectedIndex = (index != -1 ? index : 0);
	nameBox1.onchange();
});

self.port.on("update", function ([style, text])
{
	domainBox1.value = style.author;
	urlBox1.value = style.url;
	editBox1.value = text;
	enabled1.checked = style.enabled;
});

self.port.on("load-file", function ([name, text])
{
	nameBox2.value = name;
	editBox2.value = text;
});

self.port.on("resize", function ()
{
	let html = document.querySelector("html");
	self.port.emit("resize", [html.offsetWidth, html.offsetHeight]);
});

nameBox1.onchange = function ()
{
	let index = nameBox1.selectedIndex;
	if( index === -1 )
	{
		return;
	}
	self.port.emit("update", index);
};

saveBtn1.onclick = function ()
{
	let index = nameBox1.selectedIndex;
	if( index === -1 )
	{
		return;
	}
	self.port.emit("save", [index, domainBox1.value, urlBox1.value, editBox1.value]);
};

delBtn1.onclick = function ()
{
	let index = nameBox1.selectedIndex;
	if( index === -1 )
	{
		return;
	}
	delStyle3.textContent = nameBox1.value;
	gotoTab(3);
};

enabled1.onclick = function ()
{
	let index = nameBox1.selectedIndex;
	if( index === -1 )
	{
		return;
	}
	self.port.emit('enabled', [index, enabled1.checked]);
};

reloadBtn1.onclick = function ()
{
	let index = nameBox1.selectedIndex;
	if( index === -1 )
	{
		return;
	}
	self.port.emit("reload", index);
};

newBtn1.onclick = function ()
{
	nameBox2.value = "";
	domainBox2.value = "";
	urlBox2.value = "";
	editBox2.value = "";
	gotoTab(2);
};

openFolder1.onclick = function ()
{
	self.port.emit("open-folder");
};

var error_timer = false;
function hide_error()
{
	if( error_timer !== false )
	{
		clearTimeout(error_timer);
		error_timer = false;
	}
	error2.textContent = "";
	error2.style.display = "none";
}
function show_error(msg)
{
	hide_error();
	error2.textContent = msg;
	error2.style.display = "";
	error_timer = setTimeout(hide_error, 3500);
}
hide_error();

saveBtn2.onclick = function ()
{
	let name = nameBox2.value.trim();
	if( name == "" )
	{
		show_error("Empty name");
		return;
	}
	for( let i = nameBox1.options.length; i > 0; i -- )
	{
		if( nameBox1.options[i - 1].text == name )
		{
			show_error("Name already exists");
			return;
		}
	}
	editBox1.value = "";
	self.port.emit("new", [name, domainBox2.value, urlBox2.value, editBox2.value]);
	nameBox1.selectedIndex = nameBox1.appendChild(new Option(name)).index;
	nameBox1.onchange();
	cancelBtn2.onclick();
};

cancelBtn2.onclick = function ()
{
	nameBox2.value = "";
	domainBox2.value = "";
	urlBox2.value = "";
	editBox2.value = "";
	hide_error();
	gotoTab(1);
};

loadFile2.onclick = function ()
{
	self.port.emit("load-file");
};

delBtn3.onclick = function ()
{
	let index = nameBox1.selectedIndex;
	if( index === -1 )
	{
		return;
	}
	self.port.emit("delete", index);
	nameBox1.remove(index);
	nameBox1.selectedIndex = (index < nameBox1.length ? index : index - 1);
	nameBox1.onchange();
	cancelBtn3.onclick();
};

cancelBtn3.onclick = function ()
{
	delStyle3.textContent = "";
	gotoTab(1);
};

for( let i of [1, 2] )
{
	let editBox = document.getElementById("editBox" + i);
	let saveBtn = document.getElementById("saveBtn" + i);
	editBox.onkeydown = function (event)
	{
		let key =
			(event.ctrlKey ? "c" : "-") +
			(event.altKey  ? "a" : "-") +
			(event.shiftKey? "s" : "-") +
			event.keyCode;
		switch( key )
		{
			case "---9": // Tab
				event.preventDefault();
				let a = this.selectionStart;
				let b = this.selectionEnd;
				let s = this.value;
				this.value = s.substring(0, a) + "\t" + s.substring(b);
				this.selectionStart = a + 1;
				this.selectionEnd = a + 1;
				break;
			case "c--83": // Ctrl+S
				event.preventDefault();
				saveBtn.onclick();
				break;
		}
	};
}
